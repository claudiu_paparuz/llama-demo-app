package com.example.books.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface BookRepository {
    Book save(Book book);

    Page<Book> search(BookQuery query, Pageable pageable);

    Book getRequiredById(Long id);

    void delete(Book book);
}
