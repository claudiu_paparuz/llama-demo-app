package com.example.books.domain;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.util.Optional;

@Value
@Builder
public class BookQuery {
    BigDecimal minPrice;
    BigDecimal maxPrice;
    String author;

    public Optional<BigDecimal> getMinPrice() {
        return Optional.ofNullable(minPrice);
    }

    public Optional<BigDecimal> getMaxPrice() {
        return Optional.ofNullable(maxPrice);
    }

    public Optional<String> getAuthor() {
        return Optional.ofNullable(author);
    }
}
