package com.example.books.infrastructure.persistance.jpa;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(considerNestedRepositories = true, basePackages = "com.example.books")
public class JpaConfig {
}
