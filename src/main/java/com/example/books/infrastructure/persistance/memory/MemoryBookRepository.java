package com.example.books.infrastructure.persistance.memory;

import com.example.books.domain.Book;
import com.example.books.domain.BookNotFoundException;
import com.example.books.domain.BookQuery;
import com.example.books.domain.BookRepository;
import lombok.val;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Repository("memoryBookRepository")
public class MemoryBookRepository implements BookRepository {
    private final List<Book> books = new ArrayList<>();

    public MemoryBookRepository() {
        this.books.addAll(Arrays.asList(
            new Book(1L, "Hyperion", "Dan Simmons", BigDecimal.valueOf(20)),
            new Book(2L, "A Fire Upon the Deep", "Vernor Vinge", BigDecimal.valueOf(13)),
            new Book(3L, "Harry Potter and the Goblet of Fire", "J. K. Rowling", BigDecimal.valueOf(31)),
            new Book(4L, "Ancillary Justice", "Ann Leckie", BigDecimal.valueOf(29)),
            new Book(5L, "A Deepness in the Sky", "Vernor Vinge", BigDecimal.valueOf(15))
        ));
    }

    @Override
    public Book save(Book book) {
        val id = books.stream().map(Book::getId).max(Comparator.comparing(Long::intValue))
            .map(value -> value + 1)
            .orElse(0L);
        book.setId(id);
        books.add(book);
        return book;
    }

    @Override
    public Page<Book> search(BookQuery query, Pageable pageable) {
        return new PageImpl<>(books.stream().filter(b -> {
            AtomicReference<Boolean> check = new AtomicReference<>(Boolean.TRUE);
            query.getMaxPrice().ifPresent(maxPrice -> check.set(check.get() && maxPrice.compareTo(b.getPrice()) >= 0));
            query.getMinPrice().ifPresent(minPrice -> check.set(check.get() && minPrice.compareTo(b.getPrice()) <= 0));
            query.getAuthor().ifPresent(author -> check.set(check.get() && b.getAuthor().equalsIgnoreCase(author)));
            return check.get();
        }).collect(Collectors.toList()));
    }

    @Override
    public Book getRequiredById(Long id) {
        return books.stream().filter(b -> b.getId().equals(id)).findAny().orElseThrow(() -> new BookNotFoundException(id));
    }

    @Override
    public void delete(Book book) {
        books.remove(book);
    }
}
