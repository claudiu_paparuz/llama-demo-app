package com.example.books.infrastructure.api;

import com.example.books.domain.BookNotFoundException;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@AllArgsConstructor
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    @SneakyThrows
    @ExceptionHandler({BookNotFoundException.class})
    public void springHandleNotFound(HttpServletResponse response, Exception e) {
        log.warn("Client error: {}", e.getMessage(), e);
        if (!response.isCommitted()) {
            response.sendError(HttpStatus.NOT_FOUND.value(), e.getMessage());
        }
    }
}
