package com.example.books.infrastructure.api;

import com.example.books.domain.Book;
import com.example.books.domain.BookQuery;
import com.example.books.domain.BookRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@RestController
@AllArgsConstructor
public class BookController {
    //    @Qualifier("memoryBookRepository")
    @Qualifier("jpaBookRepository")
    private final BookRepository repository;
    private final BookMapper mapper;

    @PostMapping("/books")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Void> create(@RequestBody @Valid CreateBookRequest request) {
        val book = repository.save(new Book(
            request.getName(),
            request.getAuthor(),
            request.getPrice()
        ));

        return ResponseEntity
            .created(ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(book.getId()).toUri())
            .build();
    }

    @DeleteMapping("/books/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable Long id) {
        val book = repository.getRequiredById(id);
        repository.delete(book);
    }

    @GetMapping("/books")
    public Page<BookDTO> get(
        @RequestParam(required = false) BigDecimal minPrice,
        @RequestParam(required = false) BigDecimal maxPrice,
        @RequestParam(required = false) String author,
        @PageableDefault Pageable pageable
    ) {
        val query = BookQuery.builder()
            .minPrice(minPrice)
            .maxPrice(maxPrice)
            .author(author)
            .build();
        return repository.search(query, pageable).map(mapper::map);
    }

    @GetMapping("/books/{id}")
    public BookDTO get(@PathVariable Long id) {
        return mapper.map(repository.getRequiredById(id));
    }

    @Data
    private static class CreateBookRequest {
        @NotBlank
        String name;
        @NotBlank
        String author;
        @NotNull
        BigDecimal price;
    }
}
