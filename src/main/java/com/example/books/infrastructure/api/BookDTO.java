package com.example.books.infrastructure.api;

import lombok.Value;

import java.math.BigDecimal;

@Value
public class BookDTO {
    Long id;
    String name;
    String author;
    BigDecimal price;
}
