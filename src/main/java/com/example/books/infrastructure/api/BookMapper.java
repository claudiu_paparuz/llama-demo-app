package com.example.books.infrastructure.api;

import com.example.books.domain.Book;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class BookMapper {

    public BookDTO map(Book book) {
        return new BookDTO(book.getId(), book.getName(), book.getAuthor(), book.getPrice());
    }
}
