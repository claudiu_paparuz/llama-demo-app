CREATE TABLE book
(
    id     BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name   VARCHAR(250)                      NOT NULL,
    author VARCHAR(250)                      NOT NULL,
    price  NUMERIC(19, 2)                    NOT NULL
);
