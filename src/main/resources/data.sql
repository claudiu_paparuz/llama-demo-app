INSERT INTO book (id, name, author, price)
VALUES (1, 'Hyperion', 'Dan Simmons', 20),
       (2, 'A Fire Upon the Deep', 'Vernor Vinge', 13),
       (3, 'Harry Potter and the Goblet of Fire', 'J. K. Rowling', 31),
       (4, 'Ancillary Justice', 'Ann Leckie', 29),
       (5, 'A Deepness in the Sky', 'Vernor Vinge', 15);
