package com.example.books.infrastructure.persistance.memory;

import com.example.books.domain.Book;
import com.example.books.domain.BookNotFoundException;
import com.example.books.domain.BookQuery;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MemoryBookRepositoryTest {
    private final MemoryBookRepository sut = new MemoryBookRepository();

    @Test
    void save() {
        Book entity = new Book(6L, "", "", BigDecimal.ZERO);
        assertEquals(entity, sut.save(entity));
    }

    @Test
    void search() {
        BookQuery query = BookQuery.builder()
            .minPrice(BigDecimal.valueOf(25))
            .build();
        List<Book> response = sut.search(query, Pageable.unpaged()).toList();
        assertEquals(2, response.size());
    }

    @Test
    void getRequiredByIdFound() {
        Book book = sut.getRequiredById(1L);
        assertEquals(1L, book.getId());
    }

    @Test
    void getRequiredByIdNotFound() {
        assertThrows(BookNotFoundException.class, () -> sut.getRequiredById(8L));
    }
}
